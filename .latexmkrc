@default_files = ('00-main.tex');
$pdf_mode = 1;
$bibtex_use = 2;
$preview_continuous_mode = 1;
$clean_ext = "glo ist tdo lof lot";
$compiling_cmd = "tmux rename-window -t Thesis:1 COMPILING";
$success_cmd   = "tmux rename-window -t Thesis:1 OK";
$failure_cmd   = "tmux rename-window -t Thesis:1 \"!!! FAILURE !!!\"";
$pdflatex_silent_switch = "-interaction=nonstopmode";
