\chapter{Introduction}
\setcounter{page}{1}
\pagenumbering{arabic}
\label{chap:intro}

\begin{quote}
``From a set of images, select the ones that contain cell-phones.''
\end{quote}

For most human beings, this is a trivial task.
This is because human beings group objects they encounter in day-to-day life into different
\textit{categories} or \textit{classes}.
They do this based on the objects appearance, functionality and the type
of interaction appropriate for a given object.
Humans also easily recognize members of these groups, even if they have never
seen the particular object before.
This capability is desirable for many computer applications as well.
Domestic robots that autonomously take care of household chores
or augmented reality systems that recognize and analyze the environment
all rely on similar principles.
The better they recognize the objects in their surroundings,
the more independently and efficiently these systems can operate.
Advancements in these fields ultimately rely on the progress in computer
vision research. \\

The general computer vision task of recognizing arbitrary objects in images is called
object categorization and the concrete computational task
of deciding what type of object is present in an image is
called object classification.
Unfortunately, recognizing objects is a very complex process
and an open problem in computer vision.
Most of the current approaches rely on recognizing colors and regions
within images to detect the presence of an object.
In recent years depth sensing devices, such as the Microsoft Kinect,
have become broadly available at affordable prices.
Researchers and enthusiasts explore the possibilities of integrating the 
available depth data into various areas of computing,
such as human-computer-interaction and computer vision.
Depth sensing devices are also expected to become more and more prevalent in 
personal computing devices.
Current examples include the latest iteration of 
the popular HTC One smart 
phone\footnote{\url{http://www.engadget.com/2014/03/25/htc-announces-the-new-one}}
or the tablets and smart phones that emerged from the Tango project at 
Google\footnote{\url{https://www.google.com/atap/projecttango/}},
that aims at utilizing depth data for various applications in consumer devices.
The foreseeable ubiquitous availability of depth data motivated the exploration
of possibilities to use this data to boost the performance of object classification
approaches.

This thesis investigates the possibilities of performing object classification
using the color images and partial three-dimensional depth data that can
be obtained from devices such as the Microsoft Kinect and
how depth and color data can be combined to create an
object classification system that extracts information about object categories
in a fast and efficient way.
Since a depth sensing device only returns information about the objects surface
that faces the sensor, such a classification system must be able to extract
suitable information from partial views of objects.
In real-world examples, the appearance of objects varies greatly.
A good classification system must further be able to cope with objects 
from different classes that appear similar,
as well as objects from a single class that have very different appearances.
It is further examined how such a classification system can be extended
to classify objects in short video sequences.

The thesis is organized as follows: Chapter~\ref{chap:fundamentals} introduces
the concepts, tasks and challenges that are related to the 
field of research of this thesis.
Chapter~\ref{chap:rel} gives an overview over the research that has been conducted
in the branch of computer vision related to this thesis.
Chapter~\ref{chap:methodology} describes the different parts that are combined
into a object classification system.
Detailed evaluation results of all components
are presented in Chapter~\ref{chap:eval},
and the thesis concludes in Chapter~\ref{chap:conclusion}.
