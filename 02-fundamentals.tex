\chapter{Fundamentals}
\label{chap:fundamentals}

\begin{quote}
  ``The goal of \textbf{computer vision} is to make useful decisions about
  real physical objects and scenes based on sensed images.''
\end{quote}

This definition of computer vision from~\cite{MAN_ALEPH000701017} includes two terms
that are of great importance for this thesis: \textit{objects} and \textit{decisions}.
The fundamental task of the approaches outlined in this thesis is to
\textit{decide} to which general category an \textit{object} depicted
in a sensed image belongs.
The area of computer vision that concerns itself with this problem
is called \textit{object categorization}.\\

One can infer from the quote above that computer vision, in contrast to other 
computer imaging disciplines like computer graphics,
has a close relationship to the ``real world''.
Computer vision is, at its core, the task to replicate and automate
vision related tasks that humans perform with the help of computers.
Many vision tasks that are trivial for humans are in fact hard problems
to solve with a computer.
A simple example is deciding if two pictures, taken from different
angles, depict the same landmark.
For a human, this is a trivial task.
For a computer on the other hand, this is very challenging.
One possible approach is to look for parts that appear similar in both images
and then deciding if enough similar parts exist as to conclude that the two
images show in fact the same landmark.
Figure~\ref{fig:matching} shows two pictures of the Mannheim Water Tower,
including highlighted parts that have been decided to be very similar
by a standard computer vision approach.\\

The description given above leaves many questions unanswered.
To understand in more detail, how such a matching approach works,
one has to know how the image parts have been chosen,
how parts of images can be compared in terms of similarity and
how it can be made sure that the compared parts are actually on the object of
interest, and not on the background.\\

The given example above describes a matching problem, but many of these concepts
are relevant for object categorization as well.
This chapter introduces some fundamental concepts that are important
for the approaches described later in this thesis.
All computer vision tasks operate on images in one form or another.
Section~\ref{sec:images} therefore introduces what a digital image
is and which properties of images are important for computer vision.
This section also describes a type of image that is not as common as a
color image, but is also an object of study in this thesis.
Section~\ref{sec:depthsensing} briefly introduces the possibilities
and devices to generate such images.
In Section~\ref{sec:obincv}, common challenges in computer vision are described
and different research areas that deal with objects in computer vision
are outlined.
Finally, Section~\ref{sec:categorization} describes the task of object categorization 
in more detail. \\

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/wtmatching}
  \captionsource{Matching image patches}
  {Matching image patches}
  {Wikimedia Commons\\
    \url{http://commons.wikimedia.org/wiki/File:Mannheim_Wasserturm.jpg}\\
    \url{http://commons.wikimedia.org/wiki/File:Mannheim_Wasserturm_20100806.jpg}}
\label{fig:matching}
\end{figure}

\section{Digital Images}
\label{sec:images}
Digital images are images that
are processable by computers and that can be displayed by electronic display devices.
Digital images are ubiquitous in day-to-day life.
They are stored in a binary representation to make them processable by computers.
This section gives a very brief introduction to the different types of digital 
images that are most common in computer vision.\\

The two fundamental types of digital images that have to be distinguished are 
raster images and vector images.
Digital images that are the output of any form of capturing device,
such as a camera or a scanner, are usually of raster type.
A raster image has a fixed width and height, the \textit{resolution}.
It can be represented as a matrix of $width \times height$ elements.
Such an element is called a \textit{pixel} and each pixel has a fixed color value.
Lowering the resolution of a raster image results in a loss of information,
since multiple pixels have to be combined into a single new pixel value.
Increasing the resolution of a raster image makes the image appear
less detailed, as the required additional pixel values must be computed
from the existing values.
This effect is called \textit{pixelation} and can be seen in 
Figure~\ref{fig:rasterimage}.
Vector images are not represented by a matrix of pixel values,
but are described in a more mathematical way, using geometric primitives such
as lines, curves and shapes.
Vector images have no fixed resolution or set pixel values, but can be thought of
as drawing instructions rather than fixed images.
This results in the possibility to scale vector graphics to arbitrary sizes,
without the loss of information or the loss of apparent quality.
Vector images are usually used for typesetting or graphics design purposes.
As vector images are of little significance to the areas of computer vision that this
thesis concerns itself with, the term ``image'' is used to refer
to digital images of raster type.

Three types of raster images are important for this thesis: greyscale images, color images
and depth images.
Raster images, as described above, can be represented as a two-dimensional matrix.
The elements in such a matrix (pixels) have different representations,
depending on the type of image.
For greyscale and depth images, a pixel usually consists of a single integer value in 
binary representation.
For color images, a single pixel usually consists of three integer values in 
binary representation, which describe the color the pixel has.

To understand the representation of color images, a very brief introduction
of color models is necessary.
A color model (also called color system) is a way of describing a 
certain color by tuples of integer numbers.
Most color models use three numbers to represent colors.
A widely used and easy to understand color model is the RGB color model,
which models any given color by three integer numbers representing the
red, green and blue color components.
As described in~\cite{MAN_ALEPH000701017}, ``the RGB color system is an
\textit{additive color system} because colors are created by adding components
to black: $(0, 0, 0)$''.
For a given color image type, the range of colors that can be described
depends on the maximum value for each color component.
A typical data structure for RGB images is \textit{8 bits per channel},
meaning that each color component is represented by an eight bit unsigned integer.
This results in a value range of $[0,255]$ for every component.
It is also possible to express an RGB color more generally with a relative
value in the range of $[0,1]$ (or as percentages in the range $[0,100]$)
for each color component.
An example of such a representation for three pixel values 
can be seen in Figure~\ref{fig:rasterimage}.
Other color models exist, most notably the \textit{subtractive} $CMY(K)$ color model.\\

\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{images/rasterimage}
  \captionsource{Raster image}
  {Scaling of a raster image, demonstrating pixelation and color values.}
  {Wikimedia Commons\\
    \url{http://commons.wikimedia.org/wiki/File:Rgb-raster-image.png}}
\label{fig:rasterimage}
\end{figure}

As described above, colors are usually represented by three-tuples of integer values,
the most common representation using eight-bit unsigned integer values.
This is sufficient to produce more distinct colors than the human eye can distinguish.
Color representations with fewer or more bits per channel exist as well.

Color values in greyscale images are usually represented by a single integer number.
An eight bit unsigned integer representation is common for greyscale images,
making it possible to represent black (0), white (255) and 254 distinct shades of gray.\\

In addition to the color value for each pixel, some image formats also allow
an additional value for opaqueness to represent (partially) translucent images.
This additional value is often referred to as the \textit{alpha channel}.\\

The last type of ``image'' that is relevant for the thesis is the depth image.
A depth image is an image-like representation of the output of a 
depth-sensing device (see Section~\ref{sec:depthsensing}).
These images also consist of a matrix with single ``pixel'' values.
These values, however, do not represent a color, but a distance.
This means that for every pixel in the matrix the distance to the depth-sensing
device is stored.
The data points usually also consist of integer values.
These values have to be associated with a unit length, such as milli- or
centimeters, to be meaningful.
To accommodate the sensors range, the data type for these data points is usually
longer than eight bit.
The Microsoft Kinect, for example, returns 16-bit values.
To display depth images on a computer screen, they can be transformed into
a color or greyscale image.
Converting such an image to an eight bit greyscale image, for example, is trivial.
Considering only the eight most significant bits of each pixel value yields
such an image.
Converting to a color image is a bit more complicated, requiring a mapping
of depth values to color values.
An example of a depth image as greyscale and color image can be seen in 
Figure~\ref{fig:depthimage}


\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/depthimage}
\captionsource{Depth image visualization}
{Visualization of a depth image as color (left) and greyscale image (right).}
{\url{http://tuxvoid.blogspot.de/2014/03/kinect-v2-developer-preview-opencv-248_16.html}}
\label{fig:depthimage}
\end{figure}

\section{Depth Sensing Devices}
\label{sec:depthsensing}
This thesis deals with object categorization (see Section~\ref{sec:categorization})
that incorporates not only information extracted from color images,
but also information about an objects three dimensional shape.
Many different computer representations of three dimensional shapes exist.
One such representation is the depth image, as described in Section~\ref{sec:images}.
Three dimensional shapes can also be represented in the form of point clouds,
wire-frame models, mesh models or computer aided design (CAD) models.
A depth-sensing device, or simply a depth sensor, is a device that creates such
representations from actual real world objects.
Some of the representations are the immediate output of such a device,
while others require sometimes considerable post-processing of the
sensed data.
Depth images are a typical output format of the class of devices that is of interest
for this thesis.

Depth-sensing devices use different methods to derive depth information from
a sensed scene.
Three of the more common methods include stereo triangulation, time-of-flight
measurement and structured light illumination.

\begin{description}
  \item[Stereo Triangulation] \hfill \\
    When a scene is captured by two (or more) cameras
    and the physical positions of the cameras are known, depth information can
    be calculated from the geometric translation of points within the images.
    This principle exploits the fact that points that are closer to the sensing
    device will have a greater translation than those farther away.
    The problem to solve is the correspondence problem:
    Finding the correspondences of points or image patches from one image
    in the other.
    The main advantage of such an approach is the independence from the
    scenes lightning conditions, which is not true for the other two approaches.
  \item[Time-Of-Flight] \hfill \\
    Time-of-flight devices employ the known speed of light
    to sense a three dimensional scene.
    A time-of-flight camera emits light particles and measures the duration until
    the particle has been reflected by a surface and returned to the device.
    Calculating depth information from the duration is trivial.
  \item[Structured Light] \hfill \\
    Structured light devices project a known pattern of e.g.
    infrared light into the scene (see Figure~\ref{fig:kinectpattern}).
    An infrared camera, the location of which relative to the 
    infrared projector must be known,
    captures the projected pattern as it is reflected by the scene.
    As the camera has a different viewing angle than the projector,
    the reflected pattern will appear distorted on object surfaces.
    With the knowledge of the pattern and the locations of the projector and camera,
    depth values can be computed from the magnitude of the distortion.
    The Microsoft Kinect sensor is an instance of a 
    depth sensing device relying on this method.
\end{description}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/kinectpattern}
  \captionsource{Structured light pattern emitted by the Kinect}
  {The structured light pattern emitted by the Microsoft Kinect}
  {Microsoft Robotics Blog\\
    \url{http://blogs.msdn.com/b/msroboticsstudio/archive/2011/11/29/kinect-for-robotics.aspx}}
\label{fig:kinectpattern}
\end{figure}

\paragraph{Microsoft Kinect}
The Microsoft Kinect is originally an input device for the Microsoft Xbox 360
video game console.
It was later also released for Microsoft Windows and for the latest iteration
of the Xbox video game console, the Xbox One.
The device has since attracted great attention from both researchers and enthusiasts,
as it constitutes reasonably powerful sensing hardware, that can be used for 
a multitude of research tasks, for a comparatively low price.

The Kinect comprises of three different sensors: A microphone array to capture sound,
a standard RGB camera that captures color images and a depth sensor that captures
depth images.
The RGB camera of the Kinect captures color images at a rate of 30 frames per second.
The captured images have a resolution of $640 \times 480$ pixels.
Color information is stored with eight bit per channel.
A high resolution option allows capturing images with a resolution of
$1280 \times 1024$ pixels at the cost of a lower frame rate of only ten 
frames per second.

The integrated depth sensor consists of a infrared light projector and an infrared 
light camera.
The sensor uses the structured light process described above.
The angular field of view of the depth sensor is 57$^{\circ}$ horizontally
and 43$^{\circ}$ vertically~\cite{Han2013}.

\section{Objects in Computer Vision}
\label{sec:obincv}
Most relevant for this thesis is the branch of computer vision that deals with
the detection and analysis of objects.
Throughout this thesis, the term ``object'' is used to describe things
that can be distinguished from their surroundings.
In that sense, persons and animals are ``objects'' too.
The next section outlines some common challenges that are encountered by all
image analyzing computer vision approaches.
The sections after that introduce and explain different branches of 
computer vision research that deal with objects, and which are related
to the field of study of this thesis.

\subsection{Common Challenges}
Analyzing objects in images is a complex task.
Every aspect of computer vision that processes information about objects in images
has to address very specific problems.
However, there are also challenges that are independent of the specific research
and are common to image processing tasks that deal with objects within images.
The fundamental challenge is the variability of appearance of objects.
This means that the same object can differ greatly in appearance in different images.
\citet{Pinz2005} describes six important ``dimensions'' of variability:
Illumination, Occlusion, Viewpoint, Scale, Background clutter and Deformation.
Illumination is the summary of lightning conditions and brightness.
Occlusion describes the portion of the object of interest that is not visible in the
image, for example because it is obscured by another object in the foreground.
Viewpoint is the angle and zoom of the camera.
It influences which and how many details of the real world object are found in the image.
Scale describes the size of the object of interest in relation to the size of the image.
Background clutter refers to the nature of the background, if it is plain and tidy or
if there are many other objects present.
Lastly, objects can appear deformed, meaning not in their ``typical'' shape, in an image.
This can be due to a skewed or rotated image, or due to
actual deformation in the depicted object, like a bent bicycle tire, for example.

The challenges imposed by these variability dimensions have to be overcome, or at least
mitigated, by any computer vision approach that is to work well on real world images.
Many procedures to minimize the impact of variability have been proposed.
Additionally, there exist more specific approaches that are invariant to one or more
dimensions of variability, such as~\cite{Lowe04distinctiveimage}.

\subsection{Image Segmentation}
\label{subsec:segmentation}
The goal of image segmentation is to divide an image into coherent regions.
The output of image segmentation is a set of regions and a mapping that assigns
every pixel of the input image to exactly one region.
\citet{Klette2014} states that ``(Image) segmentation aims at identifying
``meaningful'' segments that can be used for describing the contents
of an image, such as a segment for ``background'', segments for ``objects'',
or particular object categories \ldots~.''
One simple approach for image segmentation is \textit{region growing}.
Region growing chooses seed pixels within the image and then
iteratively analyzes surrounding pixels.
If an adjacent pixel does not exceed a certain threshold with respect to some
distance metric, it is assigned to the same region.
Otherwise it is allocated to a new region.
An example for such a metric is the difference in greyscale value.
This process is continued until all pixels of an image are assigned to a region.

\subsection{Object localization}
\label{subsec:localization}
Object localization is closely related to image segmentation.
It is the task of determining the position of an object within an image.
The result of object localization can be a bounding box around the object,
a segmentation mask to differentiate pixels that are on the object from the background,
or pixel coordinates of the center of the object.
Background clutter and occlusion are big challenges in object localization.
Object localization is oftentimes a necessary pre-processing step for other
tasks, especially for \textit{object recognition} and \textit{object categorization} 
(see Sections~\ref{subsec:recognition} and~\ref{subsec:categorization}).

\subsection{Specific Object Recognition}
\label{subsec:recognition}
Specific object recognition is also referred to as individual object recognition,
instance recognition or plainly object recognition.
In this thesis the term ``object recognition'' is used.
The task of object recognition is identifying individual objects.
This means that an object recognition approach should recognize
a specific real-world object, like ``my bike'' or ``the neighbors car''.
This is in contrast to generic object recognition or ``object categorization'',
as described in sections~\ref{subsec:categorization} and~\ref{sec:categorization},
which aims at identifying generic categories of objects.
Automatic methods for object recognition have reached a certain degree of maturity,
often outperforming humans in certain contexts.

\subsection{Generic Object Recognition}
\label{subsec:categorization}
Generic object recognition, which is also referred to as object categorization
or object classification, is the task of determining if a before unseen
object is a member of a certain ``class'' of objects.
The term \textit{generic} object recognition is sometimes used in the literature
to distinguish it from \textit{individual} object recognition.
In this thesis, the more general term \textbf{object categorization} is used.
Since this is the area of research for this thesis, object categorization
is explained in detail in Section~\ref{sec:categorization}.


\section{Object Categorization}
\label{sec:categorization}
The basic task of object categorization is to decide whether or not an
before unseen image contains an object that  belongs to a certain category.
It has to be distinguished from object recognition (Section~\ref{subsec:recognition}),
which identifies individual, real-world objects.
The output of an object categorization approach, for example, is therefore
that the object contained in an image is a car,
not that it is ``the neighbors car''.

The principal assumption of object categorization is, that objects that belong
to the same category share some characteristics that are distinguishable from
the characteristics held by objects belonging to different categories.
To decide whether an unseen object belongs to a certain category,
an object categorization approach then needs to have knowledge of these characteristics
and must be able to detect them in unseen objects.
Identifying these characteristics from the visual appearance of an object,
and storing them in a coherent and comparable way is therefore 
one of the fundamental processes of object categorization.
Such characteristics are often referred to as \textit{cues} or \textit{features}.
Different object categorization approaches can be distinguished with regards
to which cues are used, how they are extracted from image data, or
how these cues are used to differentiate between categories.

Further differences exist in the classification abilities and results of the techniques.
Some approaches can only detect a single category,
while others differentiate between multiple categories.
The output of different approaches also vary greatly.
In the simplest case, an image is only detected to contain an object 
of a specific category.
Other approaches are able to detect a bounding box around the object within the image,
while still others recognize the shape of the object in detail.

Due to the fact that objects within the same category can have great
differences in appearance and that objects belonging to different categories
can appear very similar, selecting suitable cues from visual appearance
alone is a hard problem.
This is one of the reasons why object categorization is a challenging task.
As a result, general purpose object categorization approaches 
have not yet reached a level of maturity comparable to that of object recognition.
While machines are arguably better at object recognition than humans,
object categorization remains a very hard task for computers to solve.
Virtually every object categorization approach employs 
means of machine learning to distinguish between categories.
Machine learning techniques can be described as
``computational methods using experience to improve 
performance or to make accurate predictions''~\cite{Mohri2012}.
An important aspect is the concept of experience.
This means that machine learning approaches rely 
on prior knowledge to make decisions.
More specifically, object categorization is usually treated as 
a \textit{classification} problem. \\

\textit{Classification}, in machine learning,
``assigns objects to various classes based on measured features''~\cite{Dougherty2013}.
The instrument used to perform the assignment is called a \textit{classifier}.
Measured features, in the context of object categorization, are the
aforementioned characteristics of the distinguishable categories.
How such features are measured and how they are compared is explained in more
detail in Section~\ref{subsec:imfeat}.

It should be noted that \textit{classification} is a machine learning term.
\textit{Object categorization} is a term from computer vision, that
usually incorporates a classification task.
In addition to that, object categorization also includes the necessary
pre-processing steps, such as feature detection and feature description,
which are explained later in this section.
The term \textit{class} from machine learning and \textit{category} from
computer vision are therefore somewhat interchangeable.
The machine learning terms are concisely 
described in a quote from~\cite{MAN_ALEPH000701017}:

\begin{quote}
``An ideal \textbf{class} is a set of objects having some important common
properties: in practice, a class to which an object belongs is denoted by some 
\textbf{class label}. \textbf{Classification} is a process that assigns a label to
an object according to some representation of the object`s properties.
A \textbf{classifier} is a device or algorithm that inputs an object representation
and outputs a class label.''
\end{quote}


Two terms generally associated with classification are
\textit{Training} and \textit{Testing}.
During training, sometimes also called learning, the characteristics of classes
are extracted from some form of input data.
This is closely related to the concept of experience from the definition
of machine learning above.
Testing is the process of utilizing this experience to make new decisions.
In the context of classification, such a decision is the assignment of 
an item that has not been seen before (the item was not part
of the training process) to a class.
Numerous different classification approaches and classifiers exist in the literature.
An important differentiation characteristic of classifiers in the
context of object categorization is the number of classes it can distinguish.
\textit{Binary} classifiers ``know'' only a single class and 
can only decide whether or not an item belongs to this class or not.
\textit{General} classifiers, on the other hand, can distinguish between a fixed number
of classes.
Classifiers are often also labeled as being \textit{strong} or \textit{weak}.
This differentiation refers to the performance or accuracy of a classifier.
It is intuitive that a classifier can only be useful if its results
are better than guessing.
A binary classifier therefore must decide correctly with a probability greater than
$0.5$ and a general classifier with a probability greater than $\frac{1}{n}$, 
where n is the number of classes it can distinguish.
If the performance of a classifier is considerably better than this threshold
it is called strong.
If the performance is only slightly better, it is called weak.
There exist several meta-algorithms that can combine multiple
weak classifiers into a single strong classifier.
One of the most prominent and widely used meta-algorithm is \textit{AdaBoost}
\cite{Adaboost1997} and its further developments.

Many different classification approaches exist and selecting a suitable method
for the task at hand is vital for success.
The following sections give details of important concepts of classification
when applied to the task of object categorization.

\subsection{Categories}
\label{subsec:categories}
The first step in constructing a categorization system is 
choosing the categories that are to be distinguished.
The chosen categories influence the categorization performance
and may limit the choice of suitable methodologies.
An important requirement in the context of computer vision
is that the categories can be distinguished by visual appearance.
However, humans usually categorize objects based on their functionality rather than 
visual appearance~\cite{cohen2003infant}.
This results in often times large intra-class variabilities and 
small inter-class differences.
Categorization can also happen at different levels in the semantic hierarchy
or may need to satisfy different requirements, depending on the application domain.
The category \textit{bird} may be sufficient for a general purpose
categorization system, but is not useful in a biological or even ornithological context.
The association of objects to categories by humans has been researched in
the field of psychiatry.
A study from~\citet{Rosch1976} suggests that there is a certain ``basic level''
at which categorization usually occurs.
For general purpose categorization approaches, this should be a
suitable starting point for category definition.

\subsection{Training Data}
\label{subsec:trainingdata}
Classification systems rely on experience to make new decisions.
Experience is gained by analyzing data specifically selected for this task.
This process is called training and the input data is called training data.
The training data is used to extract characteristics from each class.
In general, training data is a set of exemplary data item tuples and,
depending on the amount of supervision, a class label for each tuple.
During training, a classifier calculates some form of prediction model
to decide which class a tuple of data items belongs to.
Training data for object categorization classifiers is in most cases a set of images,
usually with an associated label for the class of object found in the image.
The number of images and the kind of images that are necessary for good
classification performance is very dependent on the methodology used.
Preparing the training data may involve high expenditure of human labor.
The term \textit{supervision} refers to the amount
of human effort necessary, in order to prepare the data for the training task.
\citet{opelt2006role} propose a ranking scheme for object categorization approaches
and describe several ``landmarks'' in the range of supervision:
A classification system is called unsupervised,
if it requires no labels for the input images.
If it is necessary for the training images to have at least labels with the category 
of the object they contain, a system is weakly supervised.
The next landmark of supervision is reached, if a rectangular bounding box
around the objects within the images is required.
A fully supervised system requires a complete segmentation
of images from the background.
Additionally, \citet{opelt2006role} identify two types of ``hidden'' supervision:
the percentage of the image that the object occupies and
the amount of images in the training set.

\subsection{Image Features}
\label{subsec:imfeat}
The term feature, in general,  refers to some form of descriptive 
information extracted from raw data.
In the context of images, features are used to create an abstract representation
of the contents of images or parts of images,
which can be compared in terms of similarity.
When multiple features are extracted from an image that contains a certain object, 
this image can then be considered a point in a multi-dimensional
\textit{feature space}.
If the same type of features are extracted from a second image 
containing another object, this image can be projected into the same feature space.
If there also exists a distance metric for this feature space,
it is possible to express the similarity of these two images
as their distance in feature space.
An object categorization approach therefore needs to select features
and associated distance measures, such that images that contain
objects from the same category have small distances in feature space,
while images containing objects from different categories have large
distances.

Two fundamental types of features for images can be differentiated:
\textit{global features} and \textit{local features}.
\citet{Lisin2005} describe the properties and advantages
of the two approaches: 

\begin{description}
  \item[Global features] \hfill \\
    Single feature vectors that are extracted from an entire
    image at once are called global features.    
    This results in a point in a high-dimensional feature space.
    Any standard classifier can then be used to classify unseen images.
    A common problem of global features is their susceptibility to training
    images with a high degree of background clutter or occlusion.
    The training images should therefore only display a single object or provide
    a good segmentation of the object from the background.

  \item[Local features] \hfill \\
    ``A local feature is an image pattern which differs 
    from its immediate neighborhood.
    It is usually associated with a change of an image property or 
    several properties simultaneously\ldots ''\cite{tuytelaars2008local}
    Local features therefore represent interesting points within an image.
    The process of selecting interest points is called feature detection.
    Many approaches exist to select such points.
    Some detectors choose points based on a fixed grid layout,
    while others utilize more complex methods to find image regions
    that are particular significant.
    A detected feature is a part of an image.
    To make such a part comparable to other detected parts,
    a feature descriptor is required, which transforms the detected
    feature into a point in a feature space and defines a distance metric
    for this feature space.
    Later in this section, feature detection and feature description is described
    in general.
    Popular approaches to detection and description are introduced
    in Sections~\ref{sec:ftdetectors} and~\ref{sec:ftdescriptors}, respectively.\\
    A major drawback of local features is the complicated handling during classification.
    For example, different images often result in different numbers of feature points.
    Standard classification methods have to be adapted to this circumstance.
    Generally, local features are more robust with respect to clutter and occlusion
    in the training images than global features.
\end{description}

For object categorization approaches, both types of features are viable candidates.
As stated above, global features are generally more susceptible to background
clutter.
However, this drawback is negligible if a good segmentation of an object
in an image is available.
In such a case, the reduced complexity of handling global features
may make them more appropriate than local features.
Some approaches also combine global and local features, trying to take
advantages of the individual strengths.

The following two paragraphs introduce the concepts of feature detection
and feature description.
It is important to note that the distinction between a feature detector
and a feature descriptor is not always clear.
There also exists no hard differentiation of these terms in the literature.
The terms are used in this thesis as follows:
A feature detector is an algorithm that mainly returns a location within the 
image that is relevant to the content.
Such a location is usually called an interest point.
Of course, some feature detectors also output additional properties
of a returned location, such as the angle and orientation of a corner.
A feature descriptor, on the other hand, also dictates a very specific
representation of such a point, allowing comparisons and matching.
Assignment to either one of the two groups may be debatable in some cases.


\subsubsection{Feature Detection}
\label{subsubsect:ftdetection}
Feature detection is the process of identifying points or regions
within images which are suitable as input for feature description 
(see Section~\ref{subsec:ftdescr}).
There is a vast number of different feature detection methods 
proposed in the literature that employ different methods of 
detecting regions and points.
The majority of feature detectors try to find \textit{salient} points or regions
in an image.
Salient regions are parts of images that attract attention in an observer
due to visual appearance, and are therefore good candidates for feature generation.
A detailed explanation of the psychological foundations and the computational
implications of attention and saliency can be found in~\cite{Frintrop2010}.
Prominent examples for salient points are edges, corners and blobs.
Blobs are regions in images that are homogeneous with respect to 
some pre-defined parameters, like brightness or color value.
Popular edge detectors, for example, include the 
Roberts~\cite{roberts1963machine},
Sobel/Prewitt~\cite{prewitt1970object}
and Canny~\cite{Canny1986}
edge detectors.
Other approaches are based on intensity or gradient distribution~\cite{Opelt2006}.
Features are usually computed from regions (the ``neighborhoods'') around interest
points, not from single pixel values.


\subsubsection{Feature Description}
\label{subsec:ftdescr}
Feature description, sometimes also referred to as feature extraction,
is the process of creating comparable representations (features)
from detected image regions (or whole images).
The input for feature description is usually the result from a feature detector,
and the task of a feature descriptor is to store detected features 
in a reproducible and comparable way.
In an object categorization framework, feature description is a necessary 
pre-processing step for both training and testing.
A feature description is only useful, if there exists a distance measure
to evaluate the similarity of the resulting features.
This distance measure is used in classification frameworks to decide
how similar features extracted from different images are.
For example, features extracted from unseen images are compared to features
encountered during training, to calculate the probability of an image belonging to a
certain class.

\subsubsection{Feature Relationships}
\label{subsubsec:ftrelationships}
The previous sections have introduced the concept of features for image data
and how features and distance metrics can principally distinguish
between images containing objects from different categories.

Apart from considering features as independent dimensions of feature space,
there also exist some object categorization approaches that describe some
relationship between these features.
A widely used relationship between features is the spatial orientation 
and distribution of individual features.
This is especially intuitive in the case of face detection, where eyes, nose and
mouth always have the same spatial relationship, but can be used for other kinds
of object recognition as well.
The aforementioned scheme by \citet{opelt2006role} also ranks object recognition
approaches with respect to geometric considerations.
Common models employ star- or tree-shaped relationships between features.
