%\addtocontents{toc}{\protect\newpage}
\chapter{Methodology}
\label{chap:methodology}
The proposed object categorization approach incorporates several different
concepts into a nearest neighbor classification system.
This chapter introduces the individual building blocks
and explains how they are interconnected in the resulting system.
Section~\ref{sec:assumptions} outlines the assumptions that were made
when setting out to create that system and
the limitations that apply to the proposed approach are discussed.
Section~\ref{sec:inpdata} introduces the dataset that is used to train
and evaluate the classifier.
The different features that are used by the categorization approach,
how they are extracted from the input data and what the representation
of these features look like is described in Section~\ref{sec:ftextract}.
Features are extracted from both color images and depth data,
and the Section is subdivided accordingly.
Section~\ref{sec:ftspace} introduces the important concept of feature space
and explains the methodologies that are used to compute similarities
between points within that space.
Finally, Section~\ref{sec:approachoverview} gives a detailed explanation of how
the different parts are combined into an object categorization
system and which parameters influence the classification performance.

\section{Assumptions and Limitations}
\label{sec:assumptions}
As explained in Chapter~\ref{chap:fundamentals},
object categorization is far from a solved problem.
Therefore, any research in this area is currently limited to a comparatively
narrow scope.
This means that any research approach has to tackle a finely specified
subset of the categorization problem.
To make research comparable, all assumptions and limitations
have to be discussed in detail.
This section presents the assumptions that are made and the 
limitations that apply for the presented categorization approach.

One of the most relevant limitations for any object categorization approach
is the number of categories it can distinguish.
Achieving a certain level of precision gets harder,
as the number of distinguishable categories increases.
This is mainly due to the fact that increasing the number of categories
also increases the probability of cases of high intra-class variability
and inter-class similarity (see Section~\ref{subsec:categories}).

This is especially true if at least some of the categories are similar
with respect to a feature used by the categorization approach.
A feature relying solely on color values may be able to differentiate between
bananas and oranges, but adding lemons to the list of categories will
result in mistakes.
Since a useful approach has to be able to distinguish between a lot
of different categories, such ``problematic'' relationships are unavoidable.
This is one of the main challenges in object categorization.

The proposed approach can distinguish between 51 different categories.
The relationships described above apply for a number of these categories,
which requires the approach to tackle and overcome the resulting challenges.
Table~\ref{tab:categories} lists the categories that can be distinguished
by the proposed approach.

Objects from these 51 categories are typically found in office and home environments,
and the proposed approach aims at categorizing objects found in indoor scenes.
This focus makes the categorization approach suitable for use
in computer vision tasks taking place in this environment.
Possible applications can be found in service robotics or augmented reality.
The approach is not suitable for very general categories and categories
primarily found in outdoor scenes, such as 
\textit{people}, \textit{animals} or \textit{cars}.
\\

The proposed approach categorizes objects by comparing features that
are extracted from the visual appearance of an object.
More specifically, it uses information about the objects color as well
as its three-dimensional shape.
The required input therefore consists of color and depth images.
These can be obtained by RGB-D sensors, such as the Microsoft Kinect,
which are described in Section~\ref{sec:depthsensing}.
This makes the approach only applicable in situations where this information is present.

The categorization approach operates on segmented (RGB) images
and point cloud data in the \textit{pcd} format
\footnote{\url{http://pointclouds.org/documentation/tutorials/pcd_file_format.php}}.
The necessary pre-processing steps include segmentation of the object
in the RGB and depth image, as well as converting the depth image to a 3D point cloud.

Image/object segmentation is a complex computer vision task,
and is not contained in the scope of this thesis.
Extensive research has been conducted in this area, and
numerous different methods have been proposed in the literature
(see Section~\ref{subsec:segmentloc}).
Converting depth images to 3D point clouds is a comparatively easy task.
Ready-to-use implementations can be found in publicly available libraries
such as the \textit{Point Cloud Library}~\footnote{\url{http://pointclouds.org}}.

\section{Input Data}
\label{sec:inpdata}
A lot of input data is required to evaluate an object categorization approach.
Constructing such a suitable dataset is an elaborate task,
requiring equipment and human labor for category and object selection,
monitoring recording conditions and labeling images
(see Section~\ref{subsec:trainingdata}).
Creating a new and sufficiently large dataset is therefore not feasible.

Instead, the ``Large-Scale Hierarchical Multi-View RGB-D Object Dataset'' is used
for training and evaluation of the performance of the proposed approach.
The dataset has been proposed by~\citet{Lai2011},
and contains 51 different categories of objects.
For each of the 51 categories, images of three to fourteen different individuals
are contained in the dataset.
For each individual, hundreds of images from different angles are included,
totaling over 200.000 color and depth images that can be used for training and testing.
An overview of the categories, the number of instances per category and the
number of images per category can be seen in Table~\ref{tab:categories}.\\


The proposed approach utilizes information from both color and depth images,
extracting features from both the color of an object as well as its three-dimensional
shape.
The dataset additionally provides a segmentation mask for every color image,
differentiating the depicted object from the background.
Color features are only extracted from the regions of the images that show the
actual object.\\

The dataset also contains a corresponding depth image for each color image.
Depth and color images were taken by a single Kinect-style device,
assuring that the depth images show the object from the 
same perspective as the color images.
Furthermore, (partial) point clouds have been computed from these depth images
and are included in the dataset as pcd files.
The point clouds are partial point clouds because they only include points
lying on the surface facing the capturing device.
These partial point clouds are used to extract visual features based on depth data.

The dataset is introduced and described in more detail in~\cite{Lai2011}.
Example categories from the dataset include different fruits and vegetables,
dishes and office equipment.
Some example images are shown in Figure~\ref{fig:sample_images}.

\begin{figure}
  \centering
  \includegraphics{images/sample_images}
  \captionsource{Sample images from the dataset.}
  {Sample images from the dataset.}
  {\citet{Lai2011}}
\end{figure}

\begin{table}[htp]
  \centering
  \input{tables/t_categories}
  \caption{Categories, instances and images in the dataset.}
\label{tab:categories}
\end{table}


\section{Feature Extraction and Description}
\label{sec:ftextract}
Common categorization approaches rely on features extracted from color
images to make classification decisions.
The approach outlined in this thesis incorporates features from both color
images and depth images.
This section describes the features that are used in the categorization approach
and how they are computed from the respective input data.
Section~\ref{subsec:ftscolor} describes the features that are extracted from the
color images of the input dataset,
while Section~\ref{subsec:ftsdepth} explains how features can be extracted
from depth data.


\subsection{Color}
\label{subsec:ftscolor}
\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/segmask}
  \captionsource{Application of a segmentation mask}
  {Application of a segmentation mask. From top to bottom: The source image, the associated segmentation mask,
    and the segmentation mask applied to the source image.}
  {\citet{Lai2011}}
\end{figure}

The dataset includes segmentation masks for each image, allowing the calculation
of color features from only those areas of an image that show the actual object.
Figure~\ref{fig:segmask} visualizes how a segmentation mask is used to
limit color feature extraction to those regions of an image that depict the
actual object.
This assures that the color feature describes the colors of an object rather than
its background.\\

As described in Section~\ref{sec:images}, the digital representation of a color
image usually stores the red, green and blue components of every pixel.
This fact is used for the first color feature, which consists of three
histograms, one for each color component.
The histograms store the relative frequency of certain value ranges for each
color component.
The number of bins per histogram can be varied to achieve different levels
of descriptiveness.
To limit the complexity of feature detection as well as the complexity of the
classification approach, only histograms with a fixed number of bins and
bins of equal width are used.
The bin width therefore directly results from the number of bins chosen
(or vice versa).
The color images from the dataset are stored in the most common format 
with 8 bits per color channel.
Experimentation has shown that histograms with 64 bins are suitable for this feature.
This results in a bin width of 4.
Fewer bins reduce the descriptiveness significantly, while more bins
increase the computational costs without significantly increasing the descriptiveness.
This feature is referred to as \textbf{rgb64}.\\

The second color feature investigated combines the three color channels into a single
histogram.
An intuitive way to achieve this, is to create a histogram with a bin for every
possible combination of the red, green and blue color component.
For an image with 8 bits per channel, this results in
$256^3 = 16,777,216$ bins.
Histograms of this size are not feasible for a categorization approach.
It is therefore necessary to reduce the amount of bins significantly,
while retaining as much descriptive power as possible.
This is achieved by mapping groups of similar color combinations
to the same histogram bin.
A histogram size of 512 bins is chosen for this feature,
which is called ``combined color'' or abbreviated to \textbf{ccol}.
Choosing 512 as the number of bins makes it necessary to map every possible
combination of values to an integer in the range $[0,511]$.
The first step in calculating the ccol feature is to create a matrix
of the same dimensions as the input image and compute this value
for every pixel.
The final histogram can then be computed from this matrix.
The \verb=C++= code used to compute the matrix of
mapped values is shown in Listing~\ref{lst:ccol}.
A vector of the three color values is extracted from the current pixel
in Line~10.
The color values are stored in blue, green, red order and are stored to
individual variables in Lines 11 to 13.
This results in three 8 bit, unsigned integer values.
To reduce the number of color combinations,
each color component is reduced to the three most significant bits
by right-shifting the bitstring by 5 (Lines 14 to 16).
The three most significant bits of every color component are combined again
into an integer number of nine bits, resulting in 512 combinations
(0 to 511).
Combining the three values is achieved by left-shifting the three values with 
different offsets into the same variable.
The three bits from the blue component are shifted by six places, the green
value by three and the red value is simply appended (Line19).
The order is arbitrary and is chosen here according to the color
storing principles of the OpenCV library\footnote{http://opencv.org/}.

\lstset{language=C++, numbers=left}
\begin{lstlisting}[float, caption=Computation of the ccol histogram,
  frame=lines, label=lst:ccol]
  /*
  * rgbframe and ccolframe are of type cv::Mat.
  * rgbframe contains an image from the dataset,
  * ccolframe is an empty matrix of the same dimensions.
  */
  for (int i=0; i<rgbframe.rows; i++)
  {
    for (int j=0; j<rgbframe.cols; j++)
    {
      cv::Vec3b pixel = rgbframe.at<cv::Vec3b> (i * rowlength + j);
      uchar b = pixel[0];
      uchar g = pixel[1];
      uchar r = pixel[2];
      uchar bs = b >> 5;
      uchar gs = g >> 5;
      uchar rs = r >> 5;
      ushort ccol = 0;
      ccol = (bs << 6) + (gs << 3) + rs;
      ccolframe.at<ushort> (i,j) = ccol;
    }
  }
\end{lstlisting}

\subsection{Depth}
\label{subsec:ftsdepth}
This section introduces features that 
are extracted from the threedimensional appearance of an object,
incorporating not only color but also depth data into the categorization approach.
The input dataset includes depth data in the form of depth images as well as 
(partial) point cloud files computed from the depth images.
The depth features presented in this section rely on the point cloud data as input.
A point cloud data file is, in its simplest form, a list
of points represented by three-dimensional coordinates.
In the input dataset, the point cloud files have been computed from segmented
depth images, and therefore the points included in every file lie on the
surface of the associated object.
An example of a rendered point cloud that depicts the partial view of a
banana is shown in Figure~\ref{fig:pcdbanana}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/pcdbanana}
  \caption[Rendered point cloud]
  {Rendered point cloud file that depicts a banana.}\label{fig:pcdbanana}
\end{figure}

The idea behind the following features is, that the three-dimensional appearance
of an object can be represented as a \textit{shape distribution}.
Shape distributions as appearance descriptors for 3D Models
were introduced by~\citet{Osada2001}.
Shape distributions calculate a representation of a three-dimensional model by repeatedly
choosing tuples of random points on the model's surface, performing geometric
calculations on these tuples and storing the distribution of values in histograms.
In the simplest case, two points are chosen and the distance between them is calculated.
After repeating this process many times and storing the distances,
one can compute the distribution of distances and store them in a histogram.
The assumption is, that such a distribution describes an object's appearance in
a distinguishable way.
An example comparison of two such histograms computed from two instances from
different object categories can be seen in Figure~\ref{fig:d2comp}.
The histograms show the relative frequencies of the dd2 feature (explained later
in this section).

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/d2comp}
  \caption[dd2 feature histograms]
  {dd2 feature histograms of depth images depicting a banana (left)
  and an apple (right).}
\label{fig:d2comp}
\end{figure}

There exist two major parameters in the calculation 
of features based on shape distributions:
The histogram size (number of bins)
and the amount of tuples to sample.
The two parameters can be chosen independently, but both may influence the 
descriptiveness of the resulting feature.
Finding a good compromise between computational complexity and descriptiveness
requires experimentation.
The depth features used in this approach use histograms with a size
of ten bins.
The depth features are each calculated from 100,000 randomly sampled point tuples 
per object.\\

The first depth feature is the \textbf{dd2} feature.
It represents the distribution of line lengths between randomly sampled point pairs.
The only restriction is that no zero-length distances are allowed,
e.g.~the pair does not consist of the same point.
The Euclidean Distance is used as the metric for distances:\\
\begin{math}
  dist(p1,p2) = \sqrt{(p1_x - p2_x)~^2 + (p1_y - p2_y)~^2 + (p1_z - p2_z)~^2 }
\end{math}
\\
From the distances, the histogram of relative frequencies is computed.
This is accomplished by identifying the largest detected value
and setting the bin width to $\frac{maxdist}{b}$, where maxdist is the largest
distance and b the number of bins.
The relative histogram is used, because the actual distance within the point cloud
is not meaningful.
The feature extraction process if visualized in Figure~\ref{fig:dd2vis}.\\
The second depth feature is called \textbf{dd3} and represents the relative frequencies
of triangular areas enclosed by three randomly chosen points.
Again, no zero values are allowed and it is assured that, in every iteration,
three distinct points are chosen.\\
The third depth feature is called \textbf{da3} and describes the distribution of angles
between three randomly chosen points.
The angle is computed between the vectors from the first to the second
and from the first to the third chosen point.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/dd2vis}
  \caption[dd2 feature extraction]
  {Visualization of the dd2 feature extraction.}\label{fig:dd2vis}
\end{figure}

\section{Feature Space and Distance Measures}
\label{sec:ftspace}
While Section~\ref{sec:ftextract} describes the different features and how they 
are computed from color and depth data,
this section demonstrates how features can be used to compare images
and measure their similarity.
Measuring how similar two images are is an important requirement for 
object categorization.
Deciding whether or not two images are \textit{equal} is an easy task.
If two images have the same resolution, it is possible to compare the
color or greyscale value of every pixel in one image with the value of 
the corresponding pixel in the second image.
If the images are not equal, however, deciding how similar they are is more complex.
One could define the similarity of two images, for example, as the ratio
of identical color values.
For most computer vision tasks, such a simple measure is not sufficient.
More sophisticated approaches rely on image features (see Section~\ref{sec:ftextract}).
Extracting features from an image results in an abstract representation of that image.
Such a representation may consist of several histograms or histogram groups,
depending on the features extracted.
Extracting the features ``rgb64'' and ``dd2'' (Section~\ref{sec:ftextract}) for example,
will result in a histogram group that contains three distinct histograms for the
rgb64 feature, and another, single histogram for the dd2 feature.
The image from which the features have been extracted can then be \textit{projected}
onto a point in a two dimensional \textit{feature space}.
The two dimensions of that feature space are the rgb64 feature and the dd2 feature.
Extracting the same set of features from a second image, allows to project the
second image onto a (different) point in the same feature space.
The similarity of two images can then be computed as the distance of the 
two projected points in feature space.
This process is illustrated in Figure~\ref{fig:projection}.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/projectionpd}
  \caption{Similarity via feature space projection.}\label{fig:projection}
\end{figure}

To compute the similarity of two images in this way requires a distance measure
for the feature space.
Such a distance measure must consider the fact that the value ranges for the different
dimensions of the feature space may not be equal.
This applies to the given example, since the values for the rgb64 dimension
consist of groups of three histograms, whereas the values for the dd2 dimension
consist of single histograms.
A global distance measure for the feature space therefore requires distance measures
for every dimension of the feature space.
If the values returned by the dimension specific distance measures are
in the same numerical interval (e.g. $[0,1]$), they can be easily combined into a 
global distance measure.
An example for such a combinatorial distance measure is the Manhattan or
``taxicab'' distance, which is the sum of all single dimensional distances.\\

Histograms are a common result of feature extractors.
A distance measure between two histograms is therefore often a required part of 
a distance measure for dimensions in feature space.
The absolute distance of two histograms $h1$ and $h2$ is given in Equation~\ref{eq:histabs}.
$B$ denotes the number of bins per histogram and $h1_b$, $h2_b$ are the values
of the $bth$ bin of the argument histograms.

\begin{equation}
  \label{eq:histabs}
  dist(h1, h2) = \sum_{b=1}^{B} \left|{h1_b - h2_b}\right|
\end{equation}

It is important to note that this notion of histogram distance is only defined
if the two histograms have the same amount of bins.
Additionally, if several histograms are to be compared, the distances among them are
only meaningful, if the sum over all bin values is equal for every histogram.
The features introduced in Section~\ref{sec:ftextract} output histograms
with relative frequencies,
for which the sum over all bins is always 1.
This makes this distance measure applicable for these features.

Some features, like the rgb64 feature, result in multiple histograms.
The distance between two such feature manifestations $f1$ and $f2$ can be 
defined as the mean of the individual histogram distances.
This distance measure between such features is given in Equation~\ref{eq:histabsmean},
where $H$ is the number of histograms in the feature, $B_h$ the number of bins
in histogram $h$ and $f1h_b$ and $f2h_b$ the values of the $bth$ bin in 
the $hth$ histogram of the argument features.
The limitations given for the single histogram distance (Equation~\ref{eq:histabs})
apply to this distance measure as well.
Additionally, $f1$ and $f2$ must be manifestations of the same feature.

\begin{equation}
  \label{eq:histabsmean}
  dist(f1, f2) = \frac{\sum_{h=1}^{H}\sum_{b=1}^{B_h} \left|{f1h_b - f2h_b}\right|}{H}
\end{equation}

The distance measures for single histograms and histogram groups given above
all return distances in the range $[0,1]$ for relative histograms.
They are therefore suitable as input for a global, combinatorial distance 
measure for every possible feature space constructed from the features 
introduced in Section~\ref{sec:ftextract}.
A simple combinatorial distance measure for two points in a given feature space is
the mean distance.
The mean distance is equal to the sum of the individual feature distances
divided by the number of features in the feature space.
The mean distance is given in Equation~\ref{eq:meand}, with $p1$ and $p2$ as
two points in feature space, $F$ the number of features per point
(which is the number of dimensions in feature space) and $p1_f$, $p2_f$ the $fth$
feature of point 1 and 2, respectively.

\begin{equation}
  \label{eq:meand}
  dist(p1, p2) = \frac{\sum_{f=1}^{F} dist(p1_f, p2_f)} {F}
\end{equation}

This distance measure gives equal weight to every feature in feature space.
In many cases, it is desirable to give certain features more or less weight
than others.
This is especially true if the descriptive power of the different features is
not the same.
This fact is taken into account by the weighted mean distance.
The weighted mean distance takes as additional input a vector of weights,
whose length is equal to the dimensionality of the feature space
(the number of features per point in feature space).
Equation~\ref{eq:weightmeand} shows the weighted mean distance
with $n$ equal to the number of features per point, $w_i$ the weight of feature
$i$ and $p1_i$, $p2_i$ the $ith$ feature of point 1 and 2, respectively.

\begin{equation}
  \label{eq:weightmeand}
  dist(p1, p2) = \frac{\sum_{i=1}^{n} w_i * dist(p1_i, p2_i)} {\sum_{i=1}^{n} w_i}
\end{equation}

The simple mean distance is the same as the weighted mean distance with all weights
being equal.

\section{The Object Categorization Approach}
\label{sec:approachoverview}
This section describes in detail the object categorization approach that is
researched in this thesis.
It operates under the assumptions and is subject to the limitations outlined
in Section~\ref{sec:assumptions}.
The categorization approach utilizes the features that are explained in
Section~\ref{sec:ftextract} and combines them with the similarity measurements
introduced in Section~\ref{sec:ftspace} into a nearest-neighbor classification
system.
The goal of the categorization approach is to output a class label,
given an unseen instance.
An instance, in this context, is the collection of (pre-processed)
data returned by an RGB-D sensor (see Section~\ref{sec:depthsensing}).
This data includes a segmented color image of an object, as well as the
point cloud data calculated from an associated depth image.

%\begin{figure}
%  %\centering
%  \includegraphics{images/knn}
%  \caption[K-nearest-neighbor classification.]
%  {k-Nearest Neighbor classification with k=3 and k=5.\\
%Source: Wikimedia Commons, accessed 10. June 2014: \url{http://commons.wikimedia.org/wiki/File:KnnClassification.svg}}
%\label{fig:knn}
%\end{figure}


\begin{figure}
  %\centering
  \includegraphics{images/knn}
  \captionsource{K-nearest-neighbor classification.}
  {k-Nearest Neighbor classification with k=3 and k=5.}
  {Wikimedia Commons\\ \url{http://commons.wikimedia.org/wiki/File:KnnClassification.svg}}
\label{fig:knn}
\end{figure}


In Section~\ref{sec:categorization} it is explained that object categorization is
usually treated as a machine learning classification task.
Many different classification methods have been researched in the field of machine
learning.
The method that is used in this thesis is called 
\textit{k-Nearest Neighbors Classification} (k-NN).
A k-Nearest Neighbors classification system operates directly in feature
space (see Section~\ref{sec:ftspace}) and outputs a class label 
by analyzing the \textit{neighborhood} of a new instance in feature space.
In the training phase of a nearest neighbor classifier, all instances from
the training data are projected into feature space.
No model (like a decision tree) is learned beforehand, which is why nearest neighbor
classification is an instance of a \textit{lazy learner}.
To classify an unseen instance, it is projected into the same feature space
as the training data.
The distances to all instances of the training data are computed via a suitable
distance measure (see Section~\ref{sec:ftspace}).
According to the parameter $k$, the $k$ nearest neighbors, which are the k
instances from the training data with the smallest distance to the new instance,
are selected.
The class labels of the selected instances from the training data are known.
The new instance is assigned the class that occurs the most among the 
classes of the k nearest neighbor.
Figure~\ref{fig:knn} shows how a k-nearest neighbor system may classify an
instance in euclidean space, depending on the chosen $k$.\\

The overall categorization system is illustrated in Figure~\ref{fig:overview}.
The training phase of the system consists of four steps:

\begin{enumerate}
  \item Data Selection
  \item Pre-processing
  \item Feature Extraction
  \item Feature Space Projection
\end{enumerate}

The first step in training the categorization system is to select suitable training
data.
The requirements for training data in the context of object categorization are
introduced in Section~\ref{subsec:trainingdata}.
For the proposed approach, the 
``Large-Scale Hierarchical Multi-View RGB-D Object Dataset'' created by \citet{Lai2011}
is used as training data.
The dataset is described in detail in Section~\ref{sec:inpdata}.
For each instance of training data, the class label for the depicted object is 
given.
In the second step, the training data is pre-processed.
Pre-processing includes segmentation of the object from the background
in both the color and depth image.
This is necessary to decrease the susceptibility to background clutter
and make sure that the categorization system is trained on the actual object
of interest.
Segmentation is sufficient for color images as pre-processing.
Depth images require an additional pre-processing step,
since the proposed approach operates on point cloud data,
not an depth images directly.
However, calculating point cloud data from depth images is a simple task.
Since image segmentation and object localization are not within the scope of this
thesis, the already processed data from the dataset is used.
In the third step, an abstract representation for each instance is calculated.
This step is called feature extraction.
This abstract representation consists of features extracted from both the
segmented color images as well as the point cloud data.
Several features that are utilized by the proposed approach are described in
Section~\ref{sec:ftextract}.
The feature extraction process results for each instance in a set of
single histograms (e.g.\ dd2 or ccol feature) and histogram groups (e.g.\ rgb64 feature).
In the last step, these sets are combined to form, for each instance,
a point in feature space.
This process is called feature space projection.
The result of the training phase is a multi-dimensional feature space,
populated with the instances from the training data.
Each instance is mapped to a point in feature space, for which the class label
(the category to which the instance belongs) is known.
The populated feature space is utilized in the classification phase
to output a class label for a new instance, for which no class label is known.\\

The classification phase is triggered by a \textit{query}.
A query occurs when data, in this case sensed color and depth information,
is passed to the classification system
with the request to assign a class label to that data.
The first steps in the classification phase are similar to the steps in the
training phase.
Several additional steps are then necessary to derive a class label.
The steps in the classification phase consist of:

\begin{enumerate}
  \item Data Input
  \item Pre-processing
  \item Feature Extraction
  \item Feature Space Projection
  \item K-Nearest Neighbor Selection
  \item Majority Vote
\end{enumerate}

In contrast to the training phase, the input data is not specifically selected,
but stems from another application or device that requires a class assignment.
The pre-processing steps are identical to the training phase.
The data must be prepared to allow a good classification decision,
including object localization and depth image processing.
From the processed data, the same features are extracted from the new instance.
This allows this new instance to be projected into the pre-populated feature space.
In the next step, \textit{k-nearest neighbor detection}, a fixed number ($k$) of
instances that are closest to the query instance are selected from the
populated feature space.
Experimentation has shown that selecting five nearest neighbors ($k=5$) yields
good results.
Which instances are selected as neighbors depends on the distance measure that is
used for the feature space.
How distance measures work and how they can be constructed is explained in 
detail in Section~\ref{sec:ftspace}.
The selected distance measure is then used to compute the distance to the instances
in the pre-populated feature space.
The $k$ previously known instances with the smallest distances to the new
instance are returned from this selection step.
Since the class labels are known for the returned instances,
an occurrence ranking of classes that appear within the $k$ nearest neighbors
can be created in the last step.
The new instance is assigned the class that occurs most frequent within this ranking.
This is called the \textit{majority vote}.
For every k-value greater one, a tie can occur among the class frequencies.
Ties are broken by assigning the class label of the instance with the smallest
distance to the new instance among the tying instances.\\

The proposed categorization approach consists of three major building blocks:
The feature set that is extracted from input data, the distance measure for the
resulting feature space and the k-Nearest Neighbor classifier.
Each of the three building blocks can be adapted in various ways to increase
the categorization performance or to suit specific needs.
The most important parameters and modification possibilities,
which are subject to evaluation in Chapter~\ref{chap:eval},
are listed here:

\begin{itemize}
  \item Feature Set
    \begin{itemize}
      \item Set of features extracted from the color images.
      \item Set of features extracted from depth data.
      \item Weighting ratio between color features
        and features extracted from depth data.
    \end{itemize}
  \item Distance Measure
    \begin{itemize}
      \item Distance Measures for individual features
      \item Combination of individual, feature-specific distance measures
        into a global distance measure for the feature space.
      \item Weighting Scheme for individual features.
    \end{itemize}
  \item K-NN Classifier
    \begin{itemize}
      \item k-Value: The number of neighbors to include in the majority vote.
    \end{itemize}
\end{itemize}


\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/overview_v2}
  \caption{Overview over the classification approach.}\label{fig:overview}
\end{figure}


\section{Utilizing Video Sequences}
\label{sec:vidseq}
In addition to classifying individually sensed depth and color images,
the proposed approach is extended to classify objects that appear
in short video sequences.
The images in the evaluation dataset are frames from videos that were shot
with 20 frames per second.
The images are numbered according to their appearance within the videos,
making it possible to treat a series of subsequent images as a video sequence.
Video sequences are incorporated into the proposed approach by splitting the
k-nearest-neighbor classification into a two step process.
In the first step, a certain number of frames from the video are classified in the
described manner and the class labels that were returned for each frame are stored.
This results in a number of class labels, one for each video frame.
In the second step, a majority vote similar to the one described in 
Section~\ref{sec:approachoverview} is performed among the class labels
from each video frame.
The video sequence is classified as containing the object that is most
common among the individually classified frames.\\

The intention of this process is to utilize additional information about an
object to increase the classification performance.
If the object is shown from varying angles or from different sides over the course
of the video,
then more information about that object is captured than with a single image.
If the classification approach predicts the correct class label more often
than any other class label,
then this is a viable approach to classify objects appearing in video sequences.


