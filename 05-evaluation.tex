\chapter{Evaluation}
\label{chap:eval}

\section{Evaluation Metrics}
There exist numerous metrics by which a classification system can be evaluated.
The most common metrics, which will be used in the evaluation of the
proposed object categorization approach, include \textit{precision},
\textit{recall} and \textit{$F_1$-score}.
This section will introduce what these metrics represent and how they are computed
from the output of a classification system. \\

Evaluation of a classification system is performed on data for which the
correct class label is known.
Such data is know as a \textit{Gold Standard}.
Evaluation is performed by classifying every item contained in the gold standard
data and comparing the output with the actual class labels.
This can be visualized in a \textit{confusion matrix}.
A confusion matrix shows for every item from the gold standard the known actual class
and the class that was assigned by the classification system.
Figure~\ref{tab:confusionexample} shows an example of a confusion matrix.
Rows represent the actual class of an object as per the gold standard,
while columns represent the class assigned by the classifier.
In the given example, the dataset consists of a total of fifteen items
from three classes: apples, bananas and oranges.
The sum of each row is the number of items in the class represented by the row label.
In this case, each class has five items in the dataset.
The sum of each column represents the number of items classified as belonging
to the class represented by the column label.
In the given example, four items were classified as apples, six as bananas
and five as oranges.
The confusion matrix of a perfect classifier is only populated on the diagonal.
Every number not on the diagonal of the confusion matrix represents false
classifications. \\

\begin{table}[htp]
  \centering
  \input{tables/conf_example}
  \caption{Confusion matrix for three categories}
\label{tab:confusionexample}
\end{table}

For each class, four important numbers can be calculated from the confusion matrix:
The \textit{true positives (tp)}, \textit{false positives (fp)},
\textit{true negatives (tn)} and \textit{false negatives (fn)}.
These values are also often arranged in a $2 \times 2$ matrix
and called a \textit{table of confusion}.
An example of such a table for the \textit{apple} class is shown in
Figure~\ref{tab:conftabexample}
True positives represent items of a certain class that were assigned the
correct class label (apples that were assigned the apple class label).
False positives are items from different classes that were assigned the label
of this class (e.g.\ bananas that were labeled apples).
False negatives are items of the investigated class that were assigned a different label
(e.g.\ apples that were labeled as oranges).
Lastly, true negatives are all items of other classes that were not assigned the
apple class label.
A Banana that was assigned the banana class label is a true negative, but also an
orange that was assigned the banana class label.\\

\begin{table}[htp]
  \centering
  \input{tables/t_conftabexample}
  \caption{Table of confusion for the \textit{apple} class 
    from Table~\ref{tab:confusionexample}}
\label{tab:conftabexample}
\end{table}

These values alone are not very meaningful.
From these values, however, more interesting metrics can be computed.
The \textit{precision} metric is the amount of items that were classified as the
given class that are actual members of the class versus the amount of
items classified as the given class that are actually members of another class.
It is therefore the probability of an item that was classified as the given class
to actually be a member of that class.
The formula to calculate the precision of a certain class is given
in Equation~\ref{eq:precision}, with $c$ being a class, $tp_c$ the true positives
of class $c$ and $fp_c$ the false positives of class $c$.

\begin{equation}
  \label{eq:precision}
  P(c) = \frac{tp_c}{tp_c + fp_c}
\end{equation}

The \textit{recall} metric measures the ratio of items of a specific class for which
the correct class label was returned.
Equation~\ref{eq:recall} shows the computation of the recall metric of class $c$,
with $tp_c$ again being the true positives for class $c$ and $fn_c$ the false negatives
for class $c$.

\begin{equation}
  \label{eq:recall}
  R(c) = \frac{tp_c}{tp_c + fn_c}
\end{equation}

For most classification systems, it is simple to achieve a good value (close to 1)
for one of the metrics at the cost of a lowering the other.
Achieving perfect recall for a specific class is trivial.
Simply returning the label of that class for every item will result in recall
value of 1, while drastically lowering the precision value.
Improving the precision value for a certain class can be achieved by only returning
that class label when the confidence is very high.
This results in a high precision value at the cost of a reduced recall value.
For some applications achieving only high recall or high precision is acceptable.
However, for most general-purpose classification systems a balanced precision/recall
ratio is desirable.\\
The \textit{$F_1$-score}, also sometimes called \textit{F-measure},
combines the precision and recall values into a single metric.
The $F_1$-score is equivalent to the harmonic mean of precision and recall.
Equation~\ref{eq:fone} shows how to compute the $F_1$-score for a specific
class $c$ from the recall ($R(c)$) and precision ($P(c)$) values for that class.

\begin{equation}
  \label{eq:fone}
  F_1(c) = \frac{2 * R(c) * P(c)}{R(c) + P(c)}
\end{equation}

As stated above, recall, precision and $F_1$-score are metrics that describe
the performance of a classifier for a particular class.
To evaluate the performance of the classifier as a whole, these values have to be
aggregated over all classes.
The average precision (AP) and average recall (AR) of a classification system $K$
can thus be computed as shown in Equations~\ref{eq:avgprecision} and~\ref{eq:avgrecall},
with $C_k$ the number of classes distinguishable by the classification system $K$,
$P(c_i)$ and $R(c_i)$ the precision and recall values for class $i$.

\begin{equation}
  \label{eq:avgprecision}
  AP(K) = \frac{\sum_{i=1}^{C_k}P(c_i)}{C_k}
\end{equation}

\begin{equation}
  \label{eq:avgrecall}
  AR(K) = \frac{\sum_{i=1}^{C_k}R(c_i)}{C_k}
\end{equation}

From the average precision and average recall, the average $F_1$-score can be computed
as shown in Equation~\ref{eq:avgfone}.

\begin{equation}
  \label{eq:avgfone}
  AF_1(K) = \frac{2 * AP(K) * AR(K)} {AP(K) + AR(K)}
\end{equation}

To give equal weight to precision and recall, the classification approach
proposed in this thesis is evaluated mostly by the average $F_1$-score, A$F_1$.


\section{Individual Feature Evaluation}
To gain an overview over the features and as a basis for subsequent
feature combination steps,
evaluation begins by analyzing the classification performance of the 
individual features proposed in Section~\ref{sec:ftdescriptors}.
The k-Value for the nearest neighbor classifier is set to five for the single 
feature evaluation.
Evaluation is performed in a leave-one-instance-out manner.
This means that every instance for every category in the dataset is evaluated
separately on all its frames.
A \textit{frame} means both the RGB image as well as the associated depth image
that was taken simultaneously and from the same perspective.
The training data for every instance consists of selected frames from every
\textit{other} instance from that category, as well as selected frames from
\textit{every} instance of all other categories.
This assures that no information about the specific instance being tested is used
in the classification system.
This means that the classification system has to actually recognize characteristics
of other instances in that category to make the classification decision.
Since for every instance there are hundreds of frames available and the difference
from one frame to the next is oftentimes small,
not every frame for every instance is used as training data.
For the single feature evaluation, every 25th frame for every instance is used
as training data.
Every instance that is tested is evaluated on all its frames to get more reliable 
results.
This results in an average of 8160 frames in the training dataset, which contains
every 25th frame from all instances from all categories, except the instance
that is currently tested.
The average number of testing frames per instance is 674.
The following two sections describe in detail the evaluation results
for both color and depth features.

\begin{figure}[htp]
  \centering
  \includegraphics{images/colorsimil}
  \captionsource{Color similarity between classes.}
  {Color similarity among the orange, pear and potato classes.}
    {\citet{Lai2011}}
\label{fig:colorsimil}
\end{figure}

\subsection{Color Features}
\label{subsec:ftevcolor}
This section presents the results of the evaluation for the color features
presented in Section~\ref{subsec:ftscolor}.

\subsubsection{rgb64}
The rgb64 feature represents the distribution of values for the individual color
components.
It consists of three histograms, one for each of the red, green and blue color
components of an image.
For each histogram the bin width is four, mapping the 256 different possible
values to 64 color groups.
The performance of the rgb64 feature is shown in Table~\ref{tab:colorprf}.
The drawback of the rgb64 feature is that it does not include information
about the actual colors that result from the color components.
In extreme cases, two images that have no color in common can result
in equal rgb64 feature manifestation, due to the distribution of
per-channel color values.
A constructed example of such a case is given in Figure~\ref{fig:rgb64eq}.
For every color channel, the value is 255 for one half of the pixels
and 0 for the other half.
This is true for both images, even though the resulting colors are very different.
This can result in high similarity values for images with very different 
actual colors


\begin{figure}
  \centering
  \includegraphics{images/rgb64_equal}
  \caption{Two images which result in equal rgb64 features.}\label{fig:rgb64eq}
\end{figure}


\subsubsection{ccol}
The ccol feature mitigates this problem by representing an image as 
the distribution of resulting colors by a single histogram.
The ccol feature maps all possible color component combinations into 
a histogram of 512 bins.
Details on the computation of the ccol feature are outlined in
Section~\ref{subsec:ftscolor}.
Unfortunately, the ccol feature performs only slightly better than the rgb64 feature.
A comparison can be seen in Table~\ref{tab:colorprf}.
The false similarity problem described in the previous section is not a big problem
in real world examples.
The greatest problem is the inter-class similarity of objects with respect
to their color.
Table~\ref{tab:ccolmiss} shows an excerpt from the confusion matrix of the ccol feature.
It shows four classes (orange, peach, pear and potato) that are often miss-classified
among each other.

\begin{table}
  \centering
\input{tables/ifeatures/ccolmissclass}
  \caption{Partial confusion matrix of the ccol feature}\label{tab:ccolmiss}
\end{table}

For example, 92\% of actual oranges were classified as either orange, peach,
pear or potato and 85\% of objects classified as oranges were among those four classes.
Figure~\ref{fig:ccolsub} shows the same data as a greyscale confusion matrix
where darker squares highlight larger classification numbers.
Some example images that illustrate the color similarity can be seen in
Figure~\ref{fig:colorsimil}.\\
These observation show clearly that a classification approach that relies
solely on the distribution of color values is not sufficient.
Such a classification system will not be able to distinguish a blue ball
from a blue coffee mug.
Good classification systems therefore need to incorporate more aspects
of visual appearance, such as interest points or three-dimensional shape.

\begin{table}[htp]
  \centering
\input{tables/ifeatures/colfts}
\caption{Performance of color features.}
\label{tab:colorprf}
\end{table}


\begin{figure}[htp]
  \centering
  \includegraphics[width=15em]{images/ccolsub}
  \caption[Confusion matrix visualization]
  {Visualization of the confusion matrix
    in Table~\ref{tab:ccolmiss}.}\label{fig:ccolsub}
\end{figure}

\subsection{Depth Features}
\label{subsec:inddepth}
This section details the results of the individual feature evaluation
for features generated from depth data.


\subsubsection{dd2}
The dd2 feature represents the distribution of relative line lengths between a 
sample of randomly chosen point pairs.
100,000 point pairs are selected as the sample size.
To investigate how histogram bin counts influence performance,
the feature was evaluated with 10, 64 and 128 bins.
Using dd2 as the only feature in a classification system results in the classification
performance shown in Table~\ref{tab:dd2prf}.
Increasing the bin size from 10 to 64 yields slightly better results,
albeit at the cost of higher computational complexity at classification time.
A third evaluation run was performed with a feature manifestation with
128 bins.
This run performed only slightly better than the 64 bin case and is
not worth the increased computational complexity.

\begin{table}[htp]
  \centering
\input{tables/ifeatures/dd2-prf}
\caption{dd2 feature performance}
\label{tab:dd2prf}
\end{table}

\subsubsection{dd3}
The dd3 feature describes a shape by the distribution of areas of
triangles between randomly selected sets of three points.
As with the dd2 feature, the sample size is set to 100,000 point tuples.
Classification performance of the dd3 feature is shown in Table~\ref{tab:dd3prf}.
The dd3 feature, in the 10 bin manifestation, has a distinctly lower classification
performance when compared to the 10 bin manifestation of the dd2 feature.
The dd3 feature, on the other hand, benefits more from an increase in bin count.
The third run with 128 bins did not show such a leap in performance.
The increased computation cost is not worth the slight performance,
just as it is the case for the dd2 feature.

\begin{table}[htp]
  \centering
\input{tables/ifeatures/dd3-prf}
\caption{dd3 feature performance}
\label{tab:dd3prf}
\end{table}

\subsubsection{da3}
The da3 feature uses the distribution of angles between sets of three randomly
chosen points to describe the visual appearance of an object.
Again, 100,000 point tuples are chosen as the sample size for each feature.
The descriptiveness of the da3 feature in its 10 bin manifestation outperforms
both the dd2 and dd3 features in their 64 bin manifestations.
Increasing the bin count to 64 results in even better performance, reaching the
40\% mark for precision, recall and the balanced $F_1$-score.
The da3 feature is therefore the most promising among the examined
depth features.
The results are shown in Table~\ref{tab:da3prf}.
As with the previous features, a third run with 128 bins did not show a noteworthy
increase in classification performance.


\begin{table}[htp]
  \centering
\input{tables/ifeatures/da3-prf}
\caption{da3 feature performance}
\label{tab:da3prf}
\end{table}

\section{K Evaluation}
An important parameter of a k nearest neighbor classification system is the
number of neighbors to consider in the majority vote, the \textit{k} value.
A k value too small makes the classification system more susceptible to outliers,
while a large k value may include neighbors that lie beyond the actual
class boundary.
Evaluation of the k-value has been performed by repeatedly running the
classification system with the aforementioned leave-one-instance-out strategy
with different values of k.
To reduce computation time, the ten bin manifestation of the da3 feature was
used for evaluation.
Every frame for each test instance was classified while the training data
in the classification system contained every 25th frame of the dataset.
In total, seven runs with k-values in the range of one to twenty have been
recorded and the results are shown in Table~\ref{tab:kvalues}.
The overall impact of the k-value is smaller than expected.
The standard deviation of the $F_1$-score in the evaluation sample is
only 0.63\%.
Best precision was achieved at $k=4$, best recall at $k=10$ and the best
balanced $F_1$-score with $k=5$.
Due to the highest $F_1$-score, using five nearest neighbors for the majority vote
is the most viable candidate for the k-value.

\begin{table}[htp]
  \centering
\input{tables/kvalues}
\caption[Performance evaluation of k-values.]
{Results of different k-values, best values in bold font.}
\label{tab:kvalues}
\end{table}

\section{Training Frames}
The k nearest neighbor classification system, as described in 
Section~\ref{sec:approachoverview}, projects the training data into a feature space
and classifies new items by calculating their distance to the items in the feature space.
The number of items in the populated feature space therefore have a great impact
on the classification speed as well as the required memory.
The computation speed is affected directly, because each additional training item
requires an additional distance computation.
Each item also requires to be stored, either in main memory or on a hard disk drive.
It is highly desirable to fit the entire populated feature space in memory to avoid
hard drive seeking, which incurs a significant classification speed penalty.
This results in the requirement for evaluating the impact of training data size
on classification accuracy to find a good compromise between classification speed
and memory on the one hand, and classification performance on the other hand.
To examine this impact, the classification systems performance is evaluated
with different training set sizes.
The used dataset contains (depth) images of the objects that were taken while
the object was rotated on a turntable.
The average number of depth and color images per instance is 692.
Therefore, the difference in perspective of two consecutive images is,
on average, only slightly bigger than $0.5$ degrees.
This means that the expected difference in informative content between 
two consecutive images is fairly low.
It seems like a viable approach to use only every $nth$ frame for each instance
as training data to reduce memory and computational requirements.
Evaluation of the impact of training set frames was performed using the da3 metric
with ten bins and a k-value of five for different training frame selections.
The results are given in Table~\ref{tab:framesets}.
The frameskip column shows which frames were used for training.
A Frameskip of 5 means that every fifth image of every instance was included
in the training dataset, and a Frameskip of 1 that all images were included.
As the training set is separately constructed for every instance that is to be tested
in a leave-one-instance-out manner,
the resulting number of training data for each test instance varies slightly.
The average number of training images that results from a given frameskip are given
in the second column.
Figure~\ref{fig:trainframes} visualizes the resulting $F_1$-score as the number
of training images increases.
For small numbers of training images, performance increases dramatically
when adding more frames to the dataset.
This increase in performance diminishes as the training set grows,
resulting in an approximately logarithmical relationship between
training set size and classification performance.
This is probably due to the aforementioned fact that more and more
very similar images are included, which add only little additional informative content.\\

From the given results it can be concluded that
the feature space should be populated with all available training data,
if computational complexity is not an issue and memory limitations do not apply.
A ``pollution'' of the feature space does not occur,
but the gains in performance become very small.
If, on the other hand, the complexity or memory consumption is of importance,
the largest dataset still feasible for the applying limitations should be chosen.
Many of the preliminary evaluations presented in this chapter were performed
with every 25th frame.
At this point, gains in performance start to diminish more strongly.
Results on such a reduced dataset are a good indicator for the expected performance
that would result from a larger training dataset.


\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/trainframes}
  \caption[Performance of different training sets]
  {Performance for training sets resulting from different frameskips,
    measured using the average $F_1$-score (\%).}
\label{fig:trainframes}
\end{figure}

\begin{table}[htp]
  \centering
  \input{tables/framesets}
  \caption{Classification performance for different framesets}
\label{tab:framesets}
\end{table}

\section{Feature Combination}
\label{sec:ftcomb}
Evaluation of the individual features has shown their general
suitability for object classification.
To utilize the individual characteristics of color and depth features
and to thereby increase the classification performance, the individual features
are incorporated into a global classification system.
This section investigates, whether giving different weights to the individual
features can increase the classification performance.
The global classification system contains a two-tiered weighting scheme.
In the first step, color and depth features are considered separately and
suitable weights among the color and depth feature groups are determined.
The second step consists of a secondary weighting scheme, that
combines the weighted color and depth feature groups into the global 
classification system.

\subsection{Weighting Scheme for Color Features}
The evaluation of individual color features in Section~\ref{subsec:ftevcolor}
has shown that their performance is very similar.
Accordingly, the combined performance of these two features was first
evaluated for the case of equal weights on a reduced training dataset
using every 25th frame.
The performance of combined color features improved upon the performance
of the individual color features in both recall and precision. 
The resulting $F_1$-score was improved from $34.1\%$ for the better ccol feature 
to $36.3\%$ for the combination of ccol and rgb64 with equal weights.
In addition to the equal weights case, the performance of a weighting scheme
with the weights adjusted according to the individual feature performance
was evaluated,
which results in only slightly adjusted weights and almost identical performance.
Adjusting the weighting scheme further in favor of one of the two
features has resulted in slightly decreased performance.
It is therefore assumed that the equal weights case represents at least
a local maximum for classification performance with weighted color features.
Exemplary results for the color feature weighting scheme evaluation are shown
in Table~\ref{tab:params_color}.

\begin{table}[htp]
  \centering
  \input{tables/params_color}
  \caption{Results of color feature weighting evaluation}
\label{tab:params_color}
\end{table}

\subsection{Weighting Scheme for Depth Features}
Evaluation of depth features has shown greater variability in the individual
feature performance.
Three different approaches were pursued to create weighting schemes
for depth features.\\
The simplest weighting scheme, and the first one evaluated, is the case
of equal weights for all features.
In the case of the ten bin manifestations of depth features,
this results in 
an average precision value of $44.5\%$, an average recall value of $45.1\%$
and an average $F_1$-score of $44.8\%$.
This represents the baseline values for future comparison.

Another possibility to derive weights for the individual features is to consider
their individual performance as evaluated in Section~\ref{subsec:inddepth}.
Weighing the depth features in their ten bin manifestations according to their
performance relative to the best of the three features, da3, results in the following
weighting scheme:
The dd3 feature is given the lowest weight with $0.22$.
The dd2 feature is weighted with $0.37$ and the da3 feature with $0.41$.
The performance is evaluated using every 25th frame as training data and a k-value
of five.
This results in average precision, recall and $F_1$-score values of 
$45.3\%$, $45.6\%$ and $45.5\%$ over all classes.
The combination of the three depth features in their ten bin manifestation
outperforms any single depth feature in its 64 bin or even 128 bin manifestation.

Using the same performance dependent weighting approach for the 64 bin feature 
manifestations yields a weighting
of $0.27$ for dd3, $0.35$ for dd2 and $0.38$ for the da3 feature.
This results in an average precision of $48.6\%$, an average recall of $48.0\%$
and an average $F_1$-score of $48.3\%$.
The performance improvement of this feature combination in comparison
to the ten bin combination is slightly lower than the increase in performance
witnessed by increasing the bin sizes of individual features. \\

To evaluate empirically if this weighting approach is suitable,
a large amount of classification runs with different parameter combinations
were additionally performed on a reduced training dataset.
The reduced dataset was created using a frameskip of 200 (see Section~\ref{sec:ftcomb}).
The set of examined  weighting schemes include every possible combination
where the individual weights are divisible by $0.1$.
This results in 66 classification runs for both the ten bin and 64 bin manifestations.
The two best and worst parameter combinations that do not contain zero
weights for the ten and 64 bin manifestations of depth features are shown
in Table~\ref{tab:paraemp}.

\begin{table}[htp]
  \centering
  \input{tables/param_empiric}
  \caption{Best and worst results of empirical parameter evaluation.}
\label{tab:paraemp}
\end{table}

Parameter combinations that contained zero values 
(i.e.\ classification runs that did not utilize all depth features)
generally performed worse than combinations that include all features.
Most importantly, there were no parameter combinations that performed better than
the best combinations in Table~\ref{tab:paraemp}.
It is also noteworthy that the best parameter combination is equal in the
ten bin and the 64 bin case.
To compare the results of the empirical method with the equal weights and performance
weights cases,
the best two resulting weighting schemes were then evaluated on the main
evaluation dataset with a frameskip of 25.
This resulted average $F_1$-scores of $47.9\%$ and $48\%$.
Additional weighting schemes were derived from the best empirical scheme,
with slightly adjusted weights that favored one of the three features,
but their evaluation did not result in better performance.\\

The classification performance of the equal weights scheme, the performance derived
weighting scheme and the best empirically determined weighting scheme for
64 bin feature manifestations are shown shown in Table~\ref{tab:params_depth64}.
Even though the difference between the evaluated weighting schemes is small,
the increase in performance that is gained by utilizing all depth features
is significant.
The best individual depth feature (da3) in its 64 bin manifestation achieved
an average $F_1$-score of $40.7\%$, while the combination of depth features
reached an average $F_1$-score of $48.6\%$.

\begin{table}[htp]
  \centering
  \input{tables/params_depth64}
  \caption{Depth feature weighting schemes for 64 bin manifestations}
\label{tab:params_depth64}
\end{table}


\subsection{Global Weighting Scheme}
After the weighting schemes among depth and color features were evaluated,
and suitable parameters were found, an additional weighting scheme to integrate
both groups into the final classification system was required.
Evaluation of the global weighting scheme was performed on a training
dataset with a frameskip of 25.
Again, the equal weights case was evaluated first to gain a general idea
of the resulting performance.
Weighting the (weighted) color and depth feature groups equally resulted
in an average precision of $60.0\%$, an average recall of $60.2\%$ and an
average $F_1$-score of $60.1\%$.
It is immediately clear that combining both feature types substantially
increases upon the performance of utilizing only either color or depth features.
Additional weighting schemes favoring one group over the other,
as well as the performance based weighting approaches from previous sections
were also evaluated.
The results are shown in Table~\ref{tab:params_global}.
The performance based approach and a weighting scheme that weighs depth features
with $60\%$ and color features with $40\%$ performed equally well.
The 60/40 weighting scheme was chosen for all subsequent evaluations.
Using this weighting scheme, and utilizing every available image in the
training data, the classification performance further increased,
resulting in an average $F_1$-score of $65.3\%$ (Table~\ref{tab:finalres}).

\begin{table}[htp]
  \centering
  \input{tables/params_global}
  \caption{Evaluation of global weighting schemes}
\label{tab:params_global}
\end{table}


\section{Video Sequences}
The proposed approach was extended to classify video sequences.
Evaluation was performed by grouping subsequent frames from the dataset
and treating them as frames of a video.
25 subsequent frames from each view were combined, resulting in an average of
four short videos per view and twelve videos per instance.
The original videos, from which the evaluation dataset was created,
were shot at 20 frames per second.
Grouping 25 frames therefore represents video sequences that are slightly longer
than one second.
Depending on the number of instances, between 36 and and 168 video sequences were
constructed per category, totaling 3,600 sequences overall.
Evaluation of video sequences uses the same metrics and was performed in the same
leave-one-instance-out manner as individual images.
The training dataset therefore never contains frames that occur in the current sequence,
or any other sequence from the same instance,
which allows a comparison of the results between the single image
approach and the video based approach.

In a first evaluation step, classification runs with the best-performing individual
depth and color features were conducted.
The da3 feature in the 64 bin manifestation achieved an average $F_1$-score
of $43.9\%$, a noticeable improvement over the individual image
classifier, which achieved $40.2\%$ for the da3 feature.
Performing classification of video sequences with the ccol feature
did not show an improvement, resulting in almost identical $F_1$-scores.
These results are shown in Table~\ref{tab:vidifts}.

\begin{table}[htp]
  \centering
  \input{tables/vidifts}
  \caption{Performance comparison: individual features.}
\label{tab:vidifts}
\end{table}


Following the promising results for the da3 feature,
a classification run with all features and the previously
determined weighting schemes was performed on a reduced training set
consisting of every 25th frame.
This resulted in a significant improvement in performance compared
to the single image classification system.
The average $F_1$-score increased from $60.2\%$ to $65.0\%$.
A similar increase in performance can be seen when all available training
data is used.
The proposed approach for individual images resulted in an average
$F_1$-score of $65.3\%$ for this case, while
the extended classification system achieved $69.6\%$.
This clearly shows that, when video data is available,
delaying the classification task and utilizing multiple
views of an object can significantly improve the accuracy of the classification
system.
Table~\ref{tab:vidvsim} shows the results of the extended classification
system for video sequences next to the results for individual images.

\begin{table}[htp]
  \centering
  \input{tables/vidvsim}
  \caption[Performance comparison: all features.]
  {Performance comparison: all features with proposed weighting scheme.}
\label{tab:vidvsim}
\end{table}

\section{Summary}
Many important aspects of a k-nearest-neighbor object classification system
that incorporates both color and depth data
were evaluated in this chapter.
Evaluation metrics that are prominent in the evaluation of classification systems
were used to make the results comparable to other approaches.
Evaluation of the individual features has shown that the proposed features are
generally suitable for a classification approach.
Their individual performance is, for most classes, already substantially
better than guessing.
Examination of bin counts for depth features has shown that, up to a certain point,
increasing the bin count can substantially increase the classification performance.
64 bins per feature is a reasonable compromise between computational complexity
and feature performance.
Fewer bins reduce the accuracy dramatically, while higher bin counts increase
computational complexity without significantly improving the performance.
Evaluation has also shown that, for individual features, depth features
have more discriminative power than color features.
The most promising depth feature, da3, revealed better performance than any color
feature.
Single features have a big spread of $F_1$-scores among the different classes,
with classes that perform very well and others that perform poorly.
Table~\ref{tab:bestworst} shows the difference between the best and worst classes
for an evaluation run of the dd2 feature.
This spread is drastically reduced by using feature combinations.\\

\begin{table}[htp]
  \centering
\input{tables/bestworst}
\caption{Spread of $F_1$-scores for different classes.}
\label{tab:bestworst}
\end{table}


It has further been shown that weighted feature combinations of the same kind outperform
any individual feature of that kind.
Several approaches to construct weighting schemes that increase the power of
feature combinations were evaluated for both color and depth features.
In the case of color features, a weighting scheme that assigns equal weights
to the ccol and the rgb64 feature has resulted in the best performance.
For depth features, weighting the da3 feature with $38\%$,
the dd2 feature with $35\%$ and the dd3 feature with $27\%$
has resulted in the best performance. \\
Combining the weighted feature groups into a global classification system
further improves the performance which underlines the importance
of utilizing diverse information for a classification problem.
Evaluation has shown that weighting the depth feature group with $60\%$ and
the color feature group with $40\%$ achieves the best results.\\
Evaluation of a large number of k-values has shown that including
five nearest neighbors in the majority vote results in the best $F_1$-score.
This value was used for all subsequent evaluation and is the suggested
value for the classification approach.\\
The impact of the training dataset size on classification performance was also
extensively examined.
As long as computational or memory limitations do not apply,
using the greatest possible set of training data leads to the best results.
In practice, such limitations usually are present and reducing the dataset
is important.
The dataset that is used for evaluation contains 300 instances of objects
from 51 classes.
Each object is depicted from three viewing angles and for each viewing angle
about 600 images exist per object.
For this very large dataset,
using every 25th image for every view of every object is a good compromise.
This dramatically reduces the training dataset size,
and therefore the computational and memory requirements,
while still performing well. \\

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{images/boxplot}
  \caption[Boxplot of $F_1$-scores]
  {Boxplot of the $F_1$-scores of all classes for
    the proposed classification system (in \%).}
\label{fig:boxplot}
\end{figure}

\begin{table}[htp]
  \centering
  \input{tables/finalres}
  \caption{Results of the proposed classification system.}
\label{tab:finalres}
\end{table}

Taking the results of all evaluations into account, the proposed approach
for object categorization is a five-nearest-neighbor classification system.
It incorporates five different features.
The ccol and rgb64 features are extracted from color images and
the da3, dd2 and dd3 features are extracted from depth images and their 64 bin
manifestations are used.
The five features are combined by a two-step weighting scheme.
In the first step, the different feature types are combined into one group for
color features and one for depth features.
The two color features are weighted equally.
The depth group uses a weighting scheme of $38\%$, $35\%$ and $27\%$
for the da3, dd2 and dd3 features.
The second step combines the two groups by weighting the depth features with $60\%$
and the color features with $40\%$.
To achieve the best classification result, all available training data is used.
The proposed classification system was finally evaluated by the described
leave-one-instance-out method.
The results are shown in Table~\ref{tab:finalres}.
A color-coded confusion matrix for the proposed approach is depicted
in Figure~\ref{fig:finalconf}.
The confusion matrix shows that the classification system performs
very well for most classes.
For several classes the system performs almost perfectly,
with precision, recall and $F-1$-score values greater than $95\%$.
Examples of such classes include the \textit{banana}, \textit{plate} and
\textit{toothbrush} classes.
The overall classification performance is influenced by few classes
for which the system performs poorly.
These are the \textit{pitcher}, \textit{camera}, 
\textit{binder} and \textit{mushroom} classes.
The mushroom class, in particular, is virtually undetectable by the system.
The Tukey boxplot in Figure~\ref{fig:boxplot} visualizes the distribution of 
$F_1$-scores for the proposed approach.
The four classes with poor performance do not lie within 1.5 interquartile range
of the first quartile
(represented by the lower whisker)
and can therefore be considered outliers.
The proposed features are unsuitable for these classes.
Considering the large amount of classes that are distinguishable by the system
and the fact that it consists of features that can be computed very fast,
the overall performance is notably good.\\

Exemplary evaluation of the extended classification system has shown
that the approach is suitable for classifying objects that occur in video sequences,
and that utilizing more views of an object significantly increases the performance.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/final_confusion}
  \caption{Confusion matrix of the proposed approach.}\label{fig:finalconf}
\end{figure}
