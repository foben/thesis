% LaTeX thesis template.
% Version 1.2
%
% Chair of Computer Science IV,
% University of Mannheim,
% Germany
%
% Based on the KOMA script classes.
% Created by Philip Mildner 2013.
% If you have any feedback or if you find errors contact me:
% mildner@informatik.uni-mannheim.de
%
% Changelog:
% 2013-06-24 Initial version.
% 
% 2013-10-30 Version 1.1
%  - Moved table captions above the table
%  - Decreased page margins
%
% 2013-11-05 Version 1.2
%  - Moved header files to separate class file
%  - Author and title are now included in PDF properties and in declaration of honour
%
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{pi4-thesis}[2013/11/05 PI4 Thesis Template V1.2]

% Option for language selection.
\let\if@langgerman\iffalse
\DeclareOption{german}{\let\if@langgerman\iftrue}

% Pass otions that are not specified above to the base class.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrbook}}

\ProcessOptions

% Load base class.
\LoadClass[a4paper,
		   toc=listof,
		   toc=bib,
		   12pt,
		   DIV=10,
                   chapterprefix=true
		   ]{scrbook}
		   
% ---------------------------------
% Title page details
\newcommand{\piivsubject}[1]{\newcommand{\insertsubject}{#1}}
\newcommand{\piivtitle}[1]{\newcommand{\inserttitle}{#1}}
\newcommand{\piivauthor}[1]{\newcommand{\insertauthor}{#1}}
\newcommand{\piivauthorbirthday}[1]{\newcommand{\insertbirthday}{#1}}
\newcommand{\piivauthorhometown}[1]{\newcommand{\inserthometown}{#1}}
\newcommand{\piivauthorimmatriculationnr}[1]{\newcommand{\insertimmatriculationnr}{#1}}
\newcommand{\piivsupervisor}[1]{\newcommand{\insertsupervisor}{#1}}

% ---------------------------------
% Package includes
%
% The following list of packages should provide you a more or less complete basis for your thesis.
% However, it might be required that you change some package options or include new ones. For each
% of the used package a very brief description is given. If you like to know more on a specific
% package I recommend that you read the package documentation at http://www.ctan.org/pkg/ (just
% search for the package name). Of course you also can include more packages if you need them.
%
% UTF-8 should be supported by all major editors. If this causes problem change it to another
% encoding (e.g., 'latin1').
\RequirePackage[utf8]{inputenc}
% Nicer fonts for your document.
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
% Brings color to your document.
\RequirePackage{color}
% Better formatting of URLs in the text.
\RequirePackage[hyphens]{url}
% Provides functions for better hyphenation of words.
\RequirePackage{hyphenat}
% Provides support for including images in your document.
\RequirePackage{graphicx}
% Support for multiple images in one figure environment.
%\RequirePackage{subfig}
% More options for tabular envrionments.
%\RequirePackage{array}
\RequirePackage{tabularx}
\RequirePackage{longtable}
% Support for listings (for displaying algorithms, source code, etc.).
\RequirePackage{listings}
% Provides todo notes that help you keep track of what you have to do.
\RequirePackage{todonotes}
% Provides the glossary function.
\RequirePackage[toc]{glossaries}
% Some small improvements for the KOMA script packages.
\RequirePackage{scrhack}

%%%%%%%% MODIFIED OPTIONS
%%% Original in first line, modified in second

% Responsible for the style of bibliography and citations.
\RequirePackage[numbers,sort&compress]{natbib}
%\RequirePackage{natbib}

% Produces clickable links within the PDF file.
%\RequirePackage[pdftex, unicode=true,bookmarks=true, hyperfootnotes=false ]{hyperref}
\RequirePackage[pdftex, unicode=true,bookmarks=true, hyperfootnotes=false,hidelinks]{hyperref}


%%%%%%% END MODIFIED


% ---------------------------------
% Language selection
\if@langgerman
	\RequirePackage[ngerman]{babel}

	\newcommand{\abstractchap}{\addchap{Zusammenfassung}}
	\newcommand{\declarationofhonorchap}{\chapter{Ehrenwörtliche Erklärung}}
 	\newcommand{\insertsupervisordetails}{Betreuer: \insertsupervisor}
	\newcommand{\insertinstitution}{Lehrstuhl für Praktische Informatik IV\\
	Prof. Dr.-Ing. W. Effelsberg\\
	\vspace{0.25cm}
	Fakultät für Wirtschaftsinformatik\\
	und Wirtschaftsmathematik\\
	\vspace{0.25cm}
	Universität Mannheim}
	\newcommand{\insertauthordetails}{\vspace{0.2cm}
	\insertauthor\\
	geboren am \insertbirthday{} in \inserthometown\\
	Matrikelnummer \insertimmatriculationnr}

	\newcommand{\insertcitydate}[2]{#1, den #2}

	\renewcommand{\lstlistlistingname}{Auflistungsverzeichnis}
	\renewcommand{\lstlistingname}{Auflistung}
\else
	\RequirePackage[english]{babel}

	\newcommand{\abstractchap}{\addchap{Abstract}}
	\newcommand{\declarationofhonorchap}{\chapter*{Ehrenw{\"o}rtliche Erkl{\"a}rung}}
 	\newcommand{\insertsupervisordetails}{Supervisor: \insertsupervisor}
	\newcommand{\insertinstitution}{Chair of Computer Science IV\\
	Prof. Dr.-Ing. W. Effelsberg\\
	\vspace{0.25cm}
	School of Business Informatics and Mathematics\\
	University of Mannheim}
	\newcommand{\insertauthordetails}{\vspace{0.2cm}
	\insertauthor\\
	born on \insertbirthday{} in \inserthometown\\
	student ID \insertimmatriculationnr}

	\newcommand{\insertcitydate}[2]{#1, #2}

	\renewcommand{\lstlistlistingname}{List of Listings}
\fi

%% Schusterjungen und Hurenkinder verhindern.
%% Siehe deutsches TeX-FAQ (6.1.3)
\clubpenalty = 10000
\widowpenalty = 10000
\displaywidowpenalty = 10000

\title{\inserttitle}
\author{\insertauthordetails}
\publishers{\insertsupervisordetails\\
\vspace{1cm}
\insertinstitution}

\AtBeginDocument
{
\hypersetup{
	pdftitle = {\inserttitle},
	pdfauthor = {\insertauthor},
	pdfsubject = {\insertsubject}
}

\pagestyle{headings}

\pagenumbering{Alph}
\maketitle

\setcounter{page}{1}
\pagenumbering{roman}
}
